#include <stdio.h>
#include <stdlib.h>
#include <string.h>

float ctof(float in);

int 
main(int argc, char *argv[]) 
{
	if (argc < 2) {
		fprintf(stderr, "Usage: ctof {temperature}\n");
		return -1;
	}

	float output = ctof(atof(argv[1]));

	fprintf(stdout, "%.2f\n", output);
	return 0;
}

float ctof(float in)
{
	return (in * (9.0 / 5.0)) + 32;
}
