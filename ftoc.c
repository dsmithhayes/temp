#include <stdio.h>
#include <stdlib.h>
#include <string.h>

float ftoc(float in);

int 
main(int argc, char *argv[]) 
{
	if (argc < 2) {
		fprintf(stderr, "Usage: ftoc {temperature}\n");
		return -1;
	}

	float output = ftoc(atof(argv[1]));

	fprintf(stdout, "%.2f\n", output);
	return 0;
}

float
ftoc(float in)
{
	return (in - 32) * (5.0 / 9.0);
}
